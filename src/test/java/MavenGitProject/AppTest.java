package MavenGitProject;

import org.testng.annotations.*;
import org.openqa.selenium.By; 
import org.openqa.selenium.WebDriver; 
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

public class AppTest {

@Test
public void Testcase(){
	// 1. Launch the browser 
	System.setProperty("webdriver.chrome.driver", "D:\\Software\\chromedriver_win32\\chromedriver.exe");
	WebDriver dr = new ChromeDriver(); 
	//2. Open the application 
	dr.get("https://google.com/"); 
	//3. Search google
	WebElement searchBox = dr.findElement(By.name("q"));
  	searchBox.sendKeys("This is my Demo Project");     
  	searchBox.submit();  
    }
}
